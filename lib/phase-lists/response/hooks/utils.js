'use strict';

// node core modules

// 3rd party modules

// internal modules

const constants = {
  PHASE_NAME: 'cache',
  ERROR_MSG: 'Unable to perform caching operations',
  HOOK_STATE: 'hookState',
};

module.exports = {
  constants,
};
