'use strict';

// node core modules

// 3rd party modules

// internal modules
const parseContextBeforeCaching = require('./parse-before-caching');
const addToCache = require('./add-to-cache');

module.exports = (cache, pluginOptions) => [
  parseContextBeforeCaching,
  addToCache(cache, pluginOptions),
];
