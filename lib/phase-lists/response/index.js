'use strict';

// node core modules

// 3rd party modules

// internal modules
const hooks = require('./hooks');

module.exports = (cache, pluginOptions) => hooks(cache, pluginOptions);
