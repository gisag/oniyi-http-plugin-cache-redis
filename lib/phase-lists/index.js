'use strict';

// node core modules

// 3rd party modules

// internal modules
const requestHooks = require('./request');
const responseHooks = require('./response');

module.exports = (cache, pluginOptions) => ({
  requestHooks: requestHooks(cache, pluginOptions),
  responseHooks: responseHooks(cache, pluginOptions),
});
