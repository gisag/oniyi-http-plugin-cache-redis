'use strict';

// node core modules

// 3rd party modules
const _ = require('lodash');

// internal modules

/**
 * Factory method that updates specific property of a context object with provided options
 *
 * @param {Object} ctx      Object that holds information about hookState and requestOptions,
 *                          and it is passed between phase list hook phase-lists
 * @param {Function} debug  Custom logger
 */
const updateContext = (ctx, debug) => (prop, options) => {
  const ctxUpdates = _.merge({}, ctx[prop], options);

  Object.assign(ctx, {
    [prop]: ctxUpdates,
  });

  debug('Context [%s] updated -> %o', prop, ctxUpdates);
};

module.exports = {
  updateContext,
};
