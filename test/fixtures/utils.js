

// node core modules

// 3rd party modules
const _ = require('lodash');
const { create } = require('@gis-ag/oniyi-http-client');

// internal modules
const initCachePlugin = require('../../lib');
const buildRedisClient = require('../../lib/build-redis-client');

const httpClientParams = {
  responsePhases: ['initial', 'cache', 'final'],
  requestPhases: ['initial', 'cache', 'final'],
};

const queryModes = {
  publicMaxAgeZero: 'publicMaxAgeZero',
  publicMaxAgeZeroToSkip: 'publicMaxAgeZeroToSkip',
  privateMaxAgeZeroToSkip: 'privateMaxAgeZeroToSkip',
  publicMaxAgeTen: 'publicMaxAgeTen',
  publicSMaxAgeTwenty: 'publicSMaxAgeTwenty',
  publicNoExpirationTime: 'publicNoExpirationTime',
  setETagLastMod: 'setETagLastMod',
  notModifiedStatusCode: 'notModifiedStatusCode',
  privateMaxAgeZero: 'privateMaxAgeZero',
  privateMaxAgeTen: 'privateMaxAgeTen',
  privateMaxAgeTwenty: 'privateMaxAgeTwenty',
  publicMaxAgeThirty: 'publicMaxAgeThirty',
  privateExpiresSet: 'privateExpiresSet',
  badJSON: 'badJSON',
  wrongContentType: 'wrongContentType',
  errorResponse: 'errorResponse',
  unsupportedStatusCode: 'unsupportedStatusCode',
  delayed: 'delayed',
  delayedWithMustRevalidate: 'delayedWithMustRevalidate',
  abortCaching: 'abortCaching',
};

const redisClient = buildRedisClient({
  host: '127.0.0.1',
  port: 6379,
  db: 9,
});

const initContext = (t) => {
  const requestOptionsJson = {
    baseUrl: 'https://cache-plugin',
    uri: '/json',
    headers: {
      accept: 'application/json',
    },
    json: true,
    skip: {
      phases: {
        requestPhases: [],
        responsePhases: [],
      },
    },
    plugins: {
      cache: {
        delayCaching: true,
        hostConfig: {
          'cache-plugin': {
            ignoreNoLastMod: true,
            storePrivate: true,
            storeNoStore: true,
          },
        },
      },
    },
  };

  const requestOptions = {
    baseUrl: 'https://cache-plugin',
    uri: '/no-json',
    headers: {
      accept: 'application/atom+xml',
    },
    skip: {
      phases: {
        requestPhases: [],
        responsePhases: [],
      },
    },
  };

  const defaultPlugin = initCachePlugin({
    ttl: 60 * 60,
    redis: {
      db: 9,
    },
    hostConfig: {
      'cache-plugin': {
        ignoreNoLastMod: true,
        storePrivate: true,
        storeNoStore: true,
      },
    },
  });

  const client = create(httpClientParams).use(defaultPlugin);
  const eventName = 'addToCache';

  const mockedUser = {
    getId: () => '123456',
  };

  _.assign(t.context, {
    initCachePlugin,
    create,
    client,
    httpClientParams,
    requestOptions,
    requestOptionsJson,
    eventName,
    mockedUser,
  });
};

module.exports = { initContext, queryModes, redisClient };
