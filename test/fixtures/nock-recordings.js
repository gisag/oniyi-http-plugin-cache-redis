

// node core modules

// 3rd party modules
const nock = require('nock');

// internal modules
const { queryModes } = require('./utils');

/* eslint-disable */

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .times(8)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, max-age=20',
    'Expires',
    'Thu, 01 Dec 1994 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .times(2)
  .query({ mode: queryModes.abortCaching })
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, max-age=20',
    'Expires',
    'Thu, 01 Dec 1994 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.publicMaxAgeZero })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, max-age=0',
    'Expires',
    'Thu, 01 Dec 1994 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.publicMaxAgeTen })
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, max-age=10', // focus here
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.publicMaxAgeThirty })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, max-age=30', // focus here
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .times(4)
  .query({ mode: queryModes.privateMaxAgeTen })
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'private, max-age=10', // focus here
    'Expires',
    'Thu, 01 Dec 1994 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .times(2)
  .query({ mode: queryModes.privateMaxAgeTwenty })
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'private, max-age=20', // focus here
    'Expires',
    'Thu, 01 Dec 1994 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.privateExpiresSet })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public',
    'Expires',
    'Thu, 01 Mar 2019 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.publicNoExpirationTime })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/no-json')
  .query({ mode: queryModes.publicSMaxAgeTwenty })
  .times(2)
  .reply(200, 'some xml data', [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/atom+xml',
    'Cache-Control',
    'public, max-age=10, s-max-age=20', // focus here
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.setETagLastMod })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, must-revalidate', // focus here
    'etag',
    '123456',
    'Last-Modified',
    'Thu, 01 Mar 2017 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.setETagLastMod })
  .times(2)
  .reply(304, '', [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, no-cache', // focus here
    'etag',
    '123456',
    'Last-Modified',
    'Thu, 01 Mar 2017 16:00:00 GMT',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.badJSON })
  .times(2)
  .reply(200, '{ bad json syntax :W@]', [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.unsupportedStatusCode })
  .times(3)
  .reply(401, {}, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, no-cache',
    'etag',
    '123456',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.delayed })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.delayedWithMustRevalidate })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, no-cache, must-revalidate',
    'etag',
    '123456',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.publicMaxAgeZeroToSkip })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'public, max-age=0',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.privateMaxAgeZeroToSkip })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'application/json; charset=UTF-8',
    'Cache-Control',
    'private, max-age=0',
  ]);

nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/json')
  .query({ mode: queryModes.wrongContentType })
  .times(2)
  .reply(200, { test: 'data' }, [
    'Content-Language',
    'en-US',
    'Content-Type',
    'text/html; charset=UTF-8',
    'Cache-Control',
    'public',
  ]);
nock('https://cache-plugin', { encodedQueryParams: true })
  .get('/no-json')
  .query({ mode: queryModes.errorResponse })
  .times(2)
  .reply(400, 'Bad Request', [
    'Content-Language',
    'en-US',
    'Content-Type',
    'text/html; charset=UTF-8',
    'Cache-Control',
    'public',
  ]);
