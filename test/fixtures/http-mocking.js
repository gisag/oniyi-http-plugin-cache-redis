

// node core modules
const path = require('path');

// 3rd party modules

// internal modules

const RECORDINGS_PATH = path.resolve(__dirname, './nock-recordings.js');

// eslint-disable-next-line global-require, import/no-dynamic-require
const mock = () => require(RECORDINGS_PATH);

module.exports = {
  mock,
};
